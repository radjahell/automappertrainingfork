﻿using System;
using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.ReverseMappingUnflattening
{
    public class ReverseMapping
    {
        public static void Perform()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Order, OrderDto>()
                   .ReverseMap();
            });

            var customer = new Customer
            {
                Name = "Bob"
            };

            var order = new Order
            {
                Customer = customer,
                Total = 15.8m
            };

            var orderDto = Mapper.Map<Order, OrderDto>(order);

            orderDto.CustomerName = "Joe";

            Mapper.Map(orderDto, order);

            order.Customer.Name.ShouldEquals("Joe");

        }
    }

    public class Order
    {
        public decimal Total { get; set; }
        public Customer Customer { get; set; }
    }

    public class Customer
    {
        public string Name { get; set; }
    }

    public class OrderDto
    {
        public decimal Total { get; set; }
        public string CustomerName { get; set; }
    }
}
