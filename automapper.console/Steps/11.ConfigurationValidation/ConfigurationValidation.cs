﻿using System;
using AutoMapper;

namespace automapper.console.Steps.ConfigurationValidation
{
    public class ConfigurationValidation
    {
        public static void Perform()
        {
            Mapper.Initialize(cfg =>
                cfg.CreateMap<Source, Destination>());

            Mapper.Configuration.AssertConfigurationIsValid();
        }
    }

    public class Source
    {
        public int SomeValue { get; set; }
    }

    public class Destination
    {
        public int SomeValuefff { get; set; }
    }
}
