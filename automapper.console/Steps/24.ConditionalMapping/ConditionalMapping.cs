﻿using System;
using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.ConditionalMapping
{
    public static class ConditionalMapping
    { 
        public static void Perform()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Foo, Bar>()
                  .ForMember(dest => dest.baz, opt => opt.Condition(src => (src.baz >= 0)));
            });
            Mapper.AssertConfigurationIsValid();

            var foo = new Foo { baz = 5 };
            var bar = Mapper.Map<Bar>(foo);


            bar.baz.ShouldEquals((uint)5);
        }
    }

    class Foo
    {
        public int baz;
    }

    class Bar
    {
        public uint baz;
    }
}
