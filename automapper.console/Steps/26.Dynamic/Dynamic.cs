﻿using System.Dynamic;
using AutoMapper;

namespace automapper.console.Steps.Dynamic
{
    public static class Dynamic
    {
        public static void Perform()
        {
            dynamic foo = new ExpandoObject();
            foo.Bar = 5;
            foo.Baz = 6;

            Mapper.Initialize(cfg => { });

            var result = Mapper.Map<Foo>(foo);
            result.Bar.ShouldEquals(5);
            result.Baz.ShouldEquals(6);

            dynamic foo2 = Mapper.Map<ExpandoObject>(result);
            foo2.Bar.ShouldEquals(5);
            foo2.Baz.ShouldEquals(6);
        }
    }

    public class Foo
    {
        public int Bar { get; set; }
        public int Baz { get; set; }
    }
}
