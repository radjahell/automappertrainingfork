﻿using System;
using automapper.console.Steps.ConditionalMapping;
using automapper.console.Steps.Construction;
using automapper.console.Steps.Projection;
using automapper.console.Steps.ValueConverter;
using automapper.console.Steps.ValueTransformers;

namespace automapper.console
{
    class Program
    {
        static void Main(string[] args)
        {
            //Projection.Perform();
            //ValueConverter.Perform();
            //ConditionalMapping.Perform();
            //ValueTransformers.Perform();
            Construction.Perform();
        }
    }
}
